<?php
namespace SmartOSC\Article\Block;
use SmartOSC\Article\Model\ResourceModel\Article\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
class ArticleDetail extends \Magento\Framework\View\Element\Template
{
    protected $_collectionFactory;
    public function __construct(Context $context, CollectionFactory $collectionFactory)
    {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function GetIDArticle()
    {
        $articles = $this->_collectionFactory->create();
        $param= $this->getRequest()->getParam('id');
        $item = $articles->getItemById($param);
        return $item->getData();
    }
}
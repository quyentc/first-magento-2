<?php
namespace SmartOSC\Article\Block;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use SmartOSC\Article\Model\ResourceModel\Article\CollectionFactory;

class Index extends Template
{
    protected $_collectionFactory;
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        ResourceConnection $resource,
        array $data = []
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_resource = $resource;
        parent::__construct(
            $context,
            $data
        );
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('All Articles'));
        if ($this->getCustomCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'custom.history.pager'
            )->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
                ->setShowPerPage(true)->setCollection(
                    $this->getCustomCollection()
                );
            $this->setChild('pager', $pager);
            $this->getCustomCollection()->load();
        }
        return $this;
    }
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    public function getCustomCollection()
    {
        $collection = $this->_collectionFactory->create();
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);
        return $collection;
    }

    public function loadAllArticles()
    {
        $collection = $this->_article->loadAllData();

        return $collection;
    }
}

<?php

namespace SmartOSC\Article\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class LoadCompleteText implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $displayText = $observer->getData('lc');
        $event = $displayText->getText() . " - Event </br>";
        $result = $displayText->setText($event . 'Execute event successfully.');

        return $result;
    }
}

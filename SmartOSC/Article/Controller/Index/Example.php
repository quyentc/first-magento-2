<?php

namespace SmartOSC\Article\Controller\Index;

use Magento\Framework\App\Action\Action;

class Example extends Action
{
    protected $title;

    public function execute()
    {
        echo $this->setTitle('Welcome');
        echo "</br>";
        echo $this->getTitle();
    }
    public function setTitle($title)
    {
        return $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }
}

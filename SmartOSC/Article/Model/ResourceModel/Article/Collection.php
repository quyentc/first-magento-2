<?php
namespace SmartOSC\Article\Model\ResourceModel\Article;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'article_id';
    protected $_eventPrefix = 'sm_article_collection';
    protected $_eventObject = 'sm_article_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'SmartOSC\Article\Model\Article',
            'SmartOSC\Article\Model\ResourceModel\Article'
        );
    }

    public function loadDataById($id)
    {
        $collection = $this->_getItemId($id);
        return $collection;
    }

}
